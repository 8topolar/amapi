#!/usr/bin/env sh

USER_SERVICES="${HOME}/.config/systemd/user"

TEMPLATE=`cat amapi.service`
SCRIPT="${PWD}/api.sh"

[ -d "$USER_SERVICES" ] || mkdir -p "$USER_SERVICES"

echo "${TEMPLATE//API/$SCRIPT}" > "${USER_SERVICES}/amapi.service"

systemctl --user enable amapi
systemctl --user start amapi
