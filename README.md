# amApi

micro [amixer](https://linux.die.net/man/1/amixer) api using [netcat](http://netcat.sourceforge.net).

## environment variables
| name | default | description |
| ---- | ------- | ----------- |
| `PORT` | `80` | port to bind the service. |
| `STEP` | `7` | unit to step when `/low` and `/high` endpoints are called. |

## install

after you have been cloned this repo, you only need to execute the following command.

`./install.sh`

then you can `start`, `stop`, `restart`, etc. your service just using the command `systemctl --user <ACTION> amapi`.

> systemctl is required.

## uninstall

`./uninstall.sh`
