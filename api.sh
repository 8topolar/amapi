#!/usr/bin/env sh

PORT=${PORT:-8000}
STEP=${STEP:-1}

FIFO="/tmp/amixer-http-$PORT"
rm -f $FIFO
mkfifo $FIFO

trap ctrl_c INT
function ctrl_c() {
    rm -f $FIFO && exit
}

while true
do
  cat $FIFO | (
    read REQ
    ENDPOINT=$( echo $REQ | cut -d" " -f2 )
    >&2 echo $REQ

    [ "$ENDPOINT" = "/favicon.ico" ] && echo -e "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\n\r\n" && exit
    [ "$ENDPOINT" = "/low" ] && VOL="${STEP}%-"
    [ "$ENDPOINT" = "/high" ] && VOL="${STEP}%+"
    [ -z "$VOL" ] && VOL="$(echo $ENDPOINT | tr -dc '0-9')%"
    [ "$VOL" = "%" ] && VOL="0%+"

    NEW_VOL=`sh -c "amixer set Master ${VOL}" | sed -n 's/.* \[\([0-9]\+%\)\].*/\1/p' | head -1 | tr -dc '0-9'`

    LENGTH=`echo $NEW_VOL | wc -c`
    echo -e "HTTP/1.1 200 OK\r\nContent-Length: ${LENGTH}\r\n\r\n${NEW_VOL}"
  ) | nc -l $PORT > $FIFO
done
