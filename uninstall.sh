#!/usr/bin/env sh

USER_SERVICES="${HOME}/.config/systemd/user"

systemctl --user stop amapi
systemctl --user disable amapi

rm "${USER_SERVICES}/amapi.service"
